/**
 * This file is part of shift.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef SHIFT_MACROS_H
#define SHIFT_MACROS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <liberate/logging.h>

// error_t logging
#define LIBLOG_ET(msg, code) LIBLOG_ERROR(msg << " // " << error_name(code) << ": " \
    << error_message(code));


/**
 * Flow control guard; if this line is reached, an exception is thrown.
 **/
#define SHIFT_FLOW_CONTROL_GUARD_WITH(msg)                      \
  {                                                                 \
    std::stringstream flowcontrol;                                  \
    std::string mess{msg};                                          \
    if (!mess.empty()) {                                            \
      flowcontrol << mess << " - ";                                 \
    }                                                               \
    flowcontrol << "Control should never have reached this line: "  \
      << __FILE__ << ":" << __LINE__;                               \
    throw exception(ERR_UNEXPECTED, flowcontrol.str());             \
  }

#define SHIFT_FLOW_CONTROL_GUARD SHIFT_FLOW_CONTROL_GUARD_WITH("")


/**
 * Support for clang/OCLint suppressions
 **/
#if defined(__clang__) and defined(OCLINT_IS_RUNNING)
#  define OCLINT_SUPPRESS(suppression) \
    __attribute__(( \
      annotate("oclint:suppress[" suppression "]") \
    ))
#else
#  define OCLINT_SUPPRESS(annotation)
#endif

#endif // guard
