/**
 * This file is part of shift.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <shift/version.h>

#include "macros.h"

namespace shift {


std::pair<std::string, std::string>
version()
{
  return std::make_pair<std::string, std::string>(SHIFT_PACKAGE_MAJOR,
      SHIFT_PACKAGE_MINOR);
}



char const * copyright_string()
{
  return
    SHIFT_PACKAGE_NAME " " SHIFT_PACKAGE_VERSION " "
      SHIFT_PACKAGE_URL "\n"
    "Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)\n"
    "This software is licensed under the terms of the GNU General Public License\n"
    "version 3.\n"
    "Other licensing options available; please contact the copyright holder for\n"
    "information."
    ;
}


char const * license_string()
{
  return
    "This program is free software: you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation, either version 3 of the License, or\n"
    "(at your option) any later version.\n"
    "\n"
    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    "GNU General Public License for more details.\n"
    "\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program.  If not, see <http://www.gnu.org/licenses/>."
    ;
}

} // namespace shift
