#!/usr/bin/env python3

def _get_env(variable, error_code, default = None):
  import sys, os
  val = os.environ.get(variable, default)
  if val is None:
    sys.stderr.write('Could not retrieve environment variable %s, aborting!\n'
            % variable)
    sys.exit(error_code)
  return val.strip()

def get_commit_branch():
  # Woodpecker. We can add more CIs here, I suppose?
  return _get_env('CI_COMMIT_BRANCH', 1, 'main')


def get_appveyor_token():
  # Should be encrypted; configure secrets
  return _get_env('APPVEYOR_TOKEN', 2)


def get_appveyor_settings():
  import yaml
  with open('.appveyor_account.yml', 'r') as fh:
    contents = yaml.safe_load(fh)
    account = contents.get('account', None)
    slug = contents.get('slug', None)
    if account is None or slug is None:
      sys.stderr.write('Could not determine appveyor settings, aborting!\n');
      sys.exit(3)
    return account, slug


def build_request(token, account, slug, branch):
  headers = {
    'content-type': 'application/json',
    'authorization': 'Bearer %s' % token,
  }
  body = {
    'accountName': account,
    'projectSlug': slug,
    'branch': branch,
  }
  import requests
  r = requests.post('https://ci.appveyor.com/api/account/jfinkhaeuser/builds', json = body, headers = headers)
  if r.status_code != 200:
    import sys
    sys.stderr.write('Server response: %s\n' % r.json())
    sys.exit(3)


def trigger_appveyor():
  import sys
  print(sys.version)
  import platform
  print(platform.version())

  branch = get_commit_branch()
  token = get_appveyor_token()
  account, slug = get_appveyor_settings()

  build_request(token, account, slug, branch)
  print('Build successfully triggered.')

if __name__ == '__main__':
  trigger_appveyor()
