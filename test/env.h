/**
 * This file is part of shift.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef SHIFT_TEST_ENV_H
#define SHIFT_TEST_ENV_H

#include <gtest/gtest.h>

#include <shift.h>

class TestEnvironment : public testing::Environment
{
public:
// FIXME  std::shared_ptr<shift::api> api;

  TestEnvironment()
// FIXME     : api{shift::api::create()}
  {
  }
};

extern TestEnvironment * test_env;

#endif // guard
