/**
 * This file is part of shift.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef SHIFT_SHIFT_H
#define SHIFT_SHIFT_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

/**
 * Which platform are we on?
 */
#if !defined(SHIFT_PLATFORM_DEFINED)
#  if defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
#    define SHIFT_WIN32
#  else
#    define SHIFT_POSIX
#  endif
#  define SHIFT_PLATFORM_DEFINED
#endif

/**
 * Anonymous structs are a C++ extension for GNU
 */
#if defined(__GNUC__)
#  define SHIFT_ANONYMOUS __extension__
#else
#  define SHIFT_ANONYMOUS
#endif

/**
 * Decide what to include globally
 **/
#if defined(SHIFT_WIN32)
// Include windows.h with minimal definitions
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN
#    define __UNDEF_LEAN_AND_MEAN
#  endif
#  define NOMINMAX
// Unicode builds
#  define UNICODE
#  define _UNICODE
#  include <windows.h>
#  include <WinDef.h>
#  ifdef __UNDEF_LEAN_AND_MEAN
#    undef WIN32_LEAN_AND_MEAN
#    undef __UNDEF_LEAN_AND_MEAN
#  endif
#endif

// Visibility macros are used by all, so they must come first.
#include <shift/visibility.h>

// We want standard int types across the board. We also want a standard
// byte type.
#include <liberate/types.h>
namespace shift {
  using byte = ::liberate::types::byte;
} // namespace shift

// Not all, but the very basic headers are always included.
#include <shift/error.h>
#include <shift/version.h>

// TODO: include API headers if necessary

#endif // guard
