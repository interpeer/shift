/**
 * This file is part of shift.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2023 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef SHIFT_ERROR_H
#define SHIFT_ERROR_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <shift.h>

#include <string>
#include <stdexcept>

/**
 * Macros for building a table of known error codes and associated messages.
 *
 * XXX Note that the macros contain namespace definitions, but are not expanded
 *     in a namespace scope. This is to allow files that include this header to
 *     decide what namespace/scope macros are to be expanded in.
 **/
#if !defined(SHIFT_START_ERRORS)
#define SHIFT_START_ERRORS \
  namespace shift { \
  using error_t = uint32_t; \
  enum SHIFT_API : error_t {
#define SHIFT_ERRDEF(name, code, desc) name = code,
#define SHIFT_END_ERRORS \
    SHIFT_ERROR_LAST, \
    SHIFT_START_USER_RANGE = 1000 }; }
#define SHIFT_ERROR_FUNCTIONS
#endif


/*****************************************************************************
 * Error definitions
 **/
SHIFT_START_ERRORS

SHIFT_ERRDEF(ERR_SUCCESS,
    0,
    "No error")

SHIFT_ERRDEF(ERR_UNEXPECTED,
    1,
    "Nobody expects the Spanish Inquisition!")

SHIFT_ERRDEF(ERR_NOT_IMPLEMENTED,
    2,
    "This functionality is not implemented (on this platform).")

// TODO add error codes

SHIFT_END_ERRORS


#if defined(SHIFT_ERROR_FUNCTIONS)

namespace shift {

/*****************************************************************************
 * Functions
 **/

/**
 * Return the error message associated with the given error code. Never returns
 * nullptr; if an unknown error code is given, an "unidentified error" string is
 * returned. Not that this should happen, given that error_t is an enum...
 **/
SHIFT_API char const * error_message(error_t code);

/**
 * Return a string representation of the given error code. Also never returns
 * nullptr, see error_message() above.
 **/
SHIFT_API char const * error_name(error_t code);



/*****************************************************************************
 * Exception
 **/

/**
 * Exception class. Constructed with an error code and optional message;
 * wraps error_message() and error_name() above.
 **/
class SHIFT_API exception : public std::runtime_error
{
public:
  /**
   * Constructor/destructor
   **/
  explicit exception(error_t code, std::string const & details = std::string()) throw();
  virtual ~exception() throw();

  /**
   * std::exception interface
   **/
  virtual char const * what() const throw();

  /**
   * Additional functions
   **/
  char const * name() const throw();
  error_t code() const throw();

private:
  error_t     m_code;
  std::string m_message;
};


} // namespace shift

#endif // SHIFT_ERROR_FUNCTIONS


/**
 * Undefine macros again
 **/
#undef SHIFT_START_ERRORS
#undef SHIFT_ERRDEF
#undef SHIFT_END_ERRORS

#undef SHIFT_ERROR_FUNCTIONS

#endif // guard
